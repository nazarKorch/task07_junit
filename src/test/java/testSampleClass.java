import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.*;
import sample.SampleClass;

public class testSampleClass {

  @Test
  public void evenNumberShouldReturnTrue() {
    SampleClass sample = new SampleClass();
    String[] s = {"naz", "korc"};

    assertTrue(sample.isEven(4));
    assertEquals(true, sample.isEven(44));
    assertNotEquals(true, sample.isEven(101));
    for (int i = 0; i < s.length; i++) {
      assertNotNull(sample.getArrayElement(s, i));
    }
    assertThrows(Exception.class, () -> sample.getArrayElement(s, 2));
    System.out.println("asserts tested");

  }

  @Test
  public void testConstant() {
    SampleClass sample = new SampleClass();
    assertEquals(100, sample.getCons());
    System.out.println("constant tested");
  }

  @Test
  public void shouldReturnId() {
    SampleClass sample = mock(SampleClass.class);
    when(sample.getIdForName("Nazar")).thenReturn(55);
    when(sample.getIdForName(null)).thenThrow(Exception.class);

    assertEquals(55, sample.getIdForName("Nazar"));
    assertThrows(Exception.class, () -> sample.getIdForName(null));
    System.out.println("mock tested");
  }




}
