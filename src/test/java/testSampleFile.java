import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;
import sample.SampleClass;
import sample.SampleFile;

public class testSampleFile {

  @Test
  public void testVoidReturnType() {
    SampleFile sample = mock(SampleFile.class);
    doNothing().when(sample).createFile(any(String.class));
    sample.createFile("f");

    System.out.println("void  method tested");

  }
}
